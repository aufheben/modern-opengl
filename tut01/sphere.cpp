#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <SOIL/SOIL.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader-utils.h"
#include "sphere-attrib.h"

#define RAD(x) ((x) * M_PI / 180.0)

GLuint program;
GLuint vbo_sphere;
// GLuint cbo_sphere;
GLuint tbo_sphere;
GLuint ebo_sphere;
GLuint ebo_sphere2;
GLuint ebo_sphere3;
GLuint tex_sphere;
GLint attrib_coord3d;
GLint attrib_v_color;
GLint attrib_texcoord;

float fov_y    = 45.0f;

float eye_x    = 0.0f;
float eye_y    = 0.0f;
float eye_z    = 0.0f;

float theta    = 0.0f;
float phi      = 90.0f;
float center_x = 0.0f;
float center_y = 0.0f;
float center_z = 1.0f;

float up_x     = 0.0f;
float up_y     = 1.0f;
float up_z     = 0.0f;

glm::mat4 model;
glm::mat4 view;
glm::mat4 proj;

static void perspective(void);
static void lookat(void);

#if 0
static void create_triangle_strip(int center)
{
    assert(center >= 0 && center < 144);

    int center_a = center + 1;
    int middle = center_a;

    for (int i = 0; i < 72; i += 2, middle += 144)
    {
        element_indices3[i]     = middle;
        element_indices3[i + 1] = center_a == 143 ? middle - 143 : middle + 1;
    }

    middle = center_a + 144 * 35;

    for (int i = 72; i < 144; i += 2, middle -= 144)
    {
        element_indices3[i]     = middle;
        element_indices3[i + 1] = center_a == 0 ? middle + 143 : middle - 1;
    }

    int center_b = center - 1;
    middle = center_b;

    for (int i = 144; i < 216; i += 2, middle += 144)
    {
        element_indices3[i]     = middle;
        element_indices3[i + 1] = center_b == 143 ? middle - 143 : middle + 1;
    }

    middle = center_b + 144 * 35;

    for (int i = 216; i < 288; i += 2, middle -= 144)
    {
        element_indices3[i]     = middle;
        element_indices3[i + 1] = center_b == 0 ? middle + 143 : middle - 1;
    }
}
#endif

static int init_resources(void)
{
    // target GL_ARRAY_BUFFER is for vertex attributes
    glGenBuffers(1, &vbo_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere);
    glBufferData(GL_ARRAY_BUFFER, sphere_vertices_size,
                 sphere_vertices, GL_STATIC_DRAW);

    // glGenBuffers(1, &cbo_sphere);
    // glBindBuffer(GL_ARRAY_BUFFER, cbo_sphere);
    // glBufferData(GL_ARRAY_BUFFER, sphere_colors_size,
    //              sphere_colors, GL_STATIC_DRAW);

    glGenBuffers(1, &tbo_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, tbo_sphere);
    glBufferData(GL_ARRAY_BUFFER, sphere_texcoords_size,
                 sphere_texcoords, GL_STATIC_DRAW);

    glGenBuffers(1, &ebo_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere);
    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER, // target (vertex array indices)
        element_indices_size,    // size of buffer
        element_indices,         // pointer to data (to be copied)
        GL_STATIC_DRAW           // usage: upload once, draw many times
    );

    glGenBuffers(1, &ebo_sphere2);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere2);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_indices2_size,
                 element_indices2, GL_STATIC_DRAW);

    glGenBuffers(1, &ebo_sphere3);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere3);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_indices3_size,
                 element_indices3, GL_DYNAMIC_DRAW);

    int width, height;
    unsigned char *image;
    glGenTextures(1, &tex_sphere);
    glBindTexture(GL_TEXTURE_2D, tex_sphere);
    // texture stops at the last pixel when you fall off the edge
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    image = SOIL_load_image("test/2.jpg", &width, &height, NULL, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    if ((program = create_program("sphere.v.glsl", "sphere.f.glsl")) == 0)
        return 0;

    // pass each sphere vertex to the vertex shader
    attrib_coord3d  = glGetAttribLocation(program, "v_coord3d");
    // attrib_v_color  = glGetAttribLocation(program, "v_color");
    attrib_texcoord = glGetAttribLocation(program, "v_texcoord");
    if (attrib_coord3d == -1 || attrib_v_color == -1 || attrib_texcoord == -1)
    {
        fprintf(stderr, "Could not bind one of the attributes\n");
        return 0;
    }
    return 1;
}
 
static void onDisplay()
{
    // clear the background as black
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
   
    glUseProgram(program);

    // TODO: move these to init_resources or use VAO
    // Describe our vertices array to OpenGL
    glEnableVertexAttribArray(attrib_coord3d);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere);
    glVertexAttribPointer(
        attrib_coord3d,    // attribute
        3,                 // number of elements per vertex, here (x,y,z)
        GL_FLOAT,          // the type of each element
        GL_FALSE,          // take our values as-is (don't normalize)
        0,                 // no extra data between each position
        NULL               // pointer to the C array
    );

    // glEnableVertexAttribArray(attrib_v_color);
    // glBindBuffer(GL_ARRAY_BUFFER, cbo_sphere);
    // glVertexAttribPointer(attrib_v_color,  3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(attrib_texcoord);
    glBindBuffer(GL_ARRAY_BUFFER, tbo_sphere);
    glVertexAttribPointer(attrib_texcoord, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex_sphere);
    GLint uni_tex = glGetUniformLocation(program, "tex");
    glUniform1i(uni_tex, 0);

    GLint uni_model = glGetUniformLocation(program, "model");
    GLint uni_view  = glGetUniformLocation(program, "view");
    GLint uni_proj  = glGetUniformLocation(program, "proj");
    glUniformMatrix4fv(
        uni_model,            // localtion
        1,                    // count
        GL_FALSE,             // transpose
        glm::value_ptr(model) // pointer to values
    );
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glm::value_ptr(proj));

    // push each element in sphere_vertices to the vertex shader

    // int sphere_vertices_len = sizeof(sphere_vertices) / sizeof(GLfloat);
    // glDrawArrays(GL_LINE_STRIP,          // mode
    //              0,                      // skip n vertices
    //              sphere_vertices_len / 3 // number of vertices
    // );

    GLint uni_draw_lines = glGetUniformLocation(program, "draw_lines");
    glUniform1i(uni_draw_lines, 0);

#if 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere);
    glDrawElements(
        GL_TRIANGLE_STRIP,   // mode
        element_indices_num, // number of elements
        GL_UNSIGNED_SHORT,   // type of values
        NULL                 // pointer to the indices
    );
#else
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere3);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_indices3_size,
                 element_indices3, GL_DYNAMIC_DRAW);
    glDrawElements(GL_TRIANGLE_STRIP, element_indices3_num, GL_UNSIGNED_SHORT, NULL);
#endif

#if 1
    glUniform1i(uni_draw_lines, 1);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_sphere2);
    glDrawElements(GL_LINES, element_indices2_num, GL_UNSIGNED_SHORT, NULL);
#endif

    glDisableVertexAttribArray(attrib_coord3d);
    // glDisableVertexAttribArray(attrib_v_color);
    glDisableVertexAttribArray(attrib_texcoord);
    
    // display the result
    glutSwapBuffers();
}
 
static void onSpecial(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_LEFT:
            phi -= 1.0f;
            lookat();
            break;
        case GLUT_KEY_RIGHT:
            phi += 1.0f;
            lookat();
            break;
        case GLUT_KEY_UP:
            theta += 1.0f;
            lookat();
            break;
        case GLUT_KEY_DOWN:
            theta -= 1.0f;
            lookat();
            break;
        case GLUT_KEY_PAGE_UP:
            fov_y -= 1.0f;
            perspective();
            break;
        case GLUT_KEY_PAGE_DOWN:
            fov_y += 1.0f;
            perspective();
            break;
        case GLUT_KEY_HOME:
            eye_y += 0.1f;
            lookat();
            break;
        case GLUT_KEY_END:
            eye_y -= 0.1f;
            lookat();
            break;
        case GLUT_KEY_F1:
            break;
    }
    printf("eye    (%.3f, %.3f, %.3f)\n"
           "center (%.3f, %.3f, %.3f)\n"
           "up     (%.3f, %.3f, %.3f)\n"
           "fov_y  %.3f\n\n",
           eye_x, eye_y, eye_z, center_x, center_y, center_z,
           up_x, up_y, up_z, fov_y);
}

static void lookat(void)
{
    center_x = cos(RAD(theta)) * cos(RAD(phi));
    center_y = sin(RAD(theta));
    center_z = cos(RAD(theta)) * sin(RAD(phi));

    view = glm::lookAt(
        glm::vec3(eye_x, eye_y, eye_z),
        glm::vec3(center_x, center_y, center_z),
        glm::vec3(up_x, up_y, up_z)
    );

#if 0
    float percent = phi / 360.0f;
    int center = (int) (144 * percent) % 144;
    create_triangle_strip(center);
#endif
}

static void perspective(void)
{
    proj = glm::perspective(
        glm::radians(fov_y), // fovy
        800.0f / 600.0f,     // aspect
        0.1f,                // zNear (from viewer to the near clipping plane)
        10.0f                // zFar
    );
}

static void free_resources()
{
    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_sphere);
    // glDeleteBuffers(1, &cbo_sphere);
    glDeleteBuffers(1, &tbo_sphere);
    glDeleteBuffers(1, &ebo_sphere);
    glDeleteBuffers(1, &ebo_sphere2);
    glDeleteBuffers(1, &ebo_sphere3);
    // TODO: delete shaders and textures
}

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitContextVersion(2,0);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Sphere");

    GLenum glew_status = glewInit();
    if (glew_status != GLEW_OK)
    {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
        return EXIT_FAILURE;
    }

    init_sphere_attrib(3264, 1152, 1636, 578, 0);
    perspective();
    lookat();

    if (init_resources())
    {
        glutDisplayFunc(onDisplay);
        glutIdleFunc(onDisplay);// TODO: glutPostRedisplay
        glutSpecialFunc(onSpecial);
        glutMainLoop();
    }

    free_resources();
    return EXIT_SUCCESS;
}

