#ifndef SPHERE_ATTRIB_H
#define SPHERE_ATTRIB_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <GL/glew.h>
#endif
#include "dll.h"

DLL_API extern const GLfloat sphere_vertices[];
DLL_API extern size_t        sphere_vertices_size;

// extern const GLfloat sphere_colors[];
// extern size_t        sphere_colors_size;

DLL_API extern GLfloat       sphere_texcoords[];
DLL_API extern size_t        sphere_texcoords_size;

DLL_API extern GLushort      element_indices[];
DLL_API extern size_t        element_indices_size;
DLL_API extern int           element_indices_num;

DLL_API extern GLushort      element_indices2[];
DLL_API extern size_t        element_indices2_size;
DLL_API extern int           element_indices2_num;

DLL_API extern GLushort      element_indices3[];
DLL_API extern size_t        element_indices3_size;
DLL_API extern int           element_indices3_num;

DLL_API void init_sphere_attrib(int width, int height, int center_x, int center_y, int type);

#ifdef __cplusplus
}
#endif

#endif
