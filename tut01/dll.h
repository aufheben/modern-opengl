#ifndef DLL_H
#define DLL_H

#ifdef WIN32
    #ifdef DLL_EXPORT
    #define DLL_API __declspec(dllexport)
    #else
    #define DLL_API __declspec(dllimport)
    #endif
#else
    #define DLL_API
#endif

#endif
