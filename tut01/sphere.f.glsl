varying vec3 f_color;
varying vec2 f_texcoord;
uniform sampler2D tex;
uniform bool draw_lines;

void main(void) {
  if (draw_lines)
      gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
  else if (f_texcoord.x < 0.0 || f_texcoord.y < 0.0 || f_texcoord.x > 1.0 || f_texcoord.y > 1.0)
      gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
  else
      gl_FragColor = texture2D(tex, f_texcoord);
}

