#include <stdio.h>
#include <math.h>

#define RAD(x) ((x) * M_PI / 180.0)

// x = cos(theta) cos(phi)
// y = sin(theta)
// z = cos(theta) sin(phi)
//
// -90 <= theta <= 90
// 0 <= phi <= 360

int main(void)
{
    printf("positions:\n");
    for (float theta = 0.0; theta < 90.0; theta += 2.5) // 36
    {
        for (float phi = 0.0; phi < 360.0; phi += 2.5) // 144
        {
            float x = cos(RAD(theta)) * cos(RAD(phi));
            float y = sin(RAD(theta));
            float z = cos(RAD(theta)) * sin(RAD(phi));

            printf("    %.3f, %.3f, %.3f,\n", x, y, z);
        }
    }
    // top point
    printf("    %.3f, %.3f, %.3f,\n", 0.0f, 1.0f, 0.0f);

    // printf("\n\ncolors:\n");
    // for (int theta = 0; theta < 90; theta += 10) // 9
    // {
    //     for (int phi = 0; phi < 360; phi += 10) // 36
    //     {
    //         float x, y, z;

    //         if ((phi / 10) % 2 == 0)
    //         {
    //             x = 0.0f;
    //             y = 0.0f;
    //             z = 0.0f;
    //         }
    //         else
    //         {
    //             x = 1.0f;
    //             y = 1.0f;
    //             z = 1.0f;
    //         }
    //         printf("%.3f, %.3f, %.3f,\n", x, y, z);
    //     }
    // }
    return 0;
}

