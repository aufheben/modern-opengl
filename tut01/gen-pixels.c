#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define RAD(x) ((x) * M_PI / 180.0)

//float center_x = 968;
//float center_y = 542;
float center_x = 649;
float center_y = 505;

int round_int(float r)
{
    return (r > 0.0) ? (r + 0.5) : (r - 0.5);
}

int main(int argc, char **argv)
{
    if (argc == 1)
        return 0;
    int n = argc -1;
    int radii[n];

    for (int i = 0; i < n; i++)
    {
        radii[i] = atoi(argv[i + 1]);
        printf("%i ", radii[i]);
    }
    printf("\n");

    for (int i = 0; i < n; i++)
    {
        for (int phi = 0; phi > -360; phi -= 5)
        {
            float x = radii[i] * cos(RAD(phi));
            float y = radii[i] * sin(RAD(phi));
            printf("    %.3f, %.3f,\n", center_x + x, center_y + y);
        }
    }
    return 0;
}

