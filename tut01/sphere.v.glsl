attribute vec3 v_coord3d;
attribute vec3 v_color;
attribute vec2 v_texcoord;
varying vec3 f_color;
varying vec2 f_texcoord;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main(void) {
  gl_Position = proj * view * model * vec4(v_coord3d, 1.0);
  f_texcoord  = v_texcoord;
  f_color     = v_color;
}

