LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := sphere
LOCAL_SRC_FILES := shader-utils.cpp sphere-attrib.cpp

include $(BUILD_STATIC_LIBRARY)
