#ifndef SHADER_UTIL_H
#define SHADER_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <GL/glew.h>
#endif
#include "dll.h"

DLL_API char  *file_read(const char *filename);
DLL_API void   print_log(GLuint object);
DLL_API GLuint create_shader(const char *filename, GLenum type);
DLL_API GLuint create_program(const char *vertexfile, const char *fragmentfile);
DLL_API GLint  get_attrib(GLuint program, const char *name);
DLL_API GLint  get_uniform(GLuint program, const char *name);

#ifdef __cplusplus
}
#endif

#endif
